//npm init
//npm install express
// .gitignore as new file then type 'node_modules' inside

//default set up  of expressJS
// "require" to load the module or package

const express = require('express');

//creation of an app/project
// in layman's terms, app is our server
//default
const app = express(); 
const port = 3000;

//set up for allowing the server to handle data from request
// default, middlewares
// handling api

// allowing system to handle api, convert to JSON()
app.use(express.json());

//allows your app to read data from forms
// allow to access the users input from browser 
// nakaka basa ng user input
app.use(express.urlencoded({extended: true}));

//[SECTIO] - ROUTES

//GET METHOD
// get method - lower case in express
app.get("/", (req, res) => {
    res.send("Hello word");
})

//POST METHOD - INPUT USER
 //METHOD
app.post("/hello", (req, res) => {
    res.send(`Hello there ${req.body.firstname} ${req.body.lastname}!`);
})

//MOCK DATABASE
// An array that will store user object when the "/signup" route is a
//accessed
//server as our mock database

let users = [];

//POST METHOD FOR SIGNUP
//server
//register a user
app.post('/signup', (req, res) => {
    console.log(req.body);

    //if not empty 
    if (req.body.username !== '' && req.body.password !== ''){
        users.push(req.body);
        res.send(`User ${req.body.username} successfully registered`);
    } else {
        res.send('Please input BOTH username and passowrd');
    }

});

//PUT/UPDATE METHOD
app.put('/change-password', (req,res) => {
    //creates a variable to store the msg to be sent back to the
    //client or browser
    let msg = '';

    //creates a foor loop that will loop through the elements of "users"
    for(let i = 0; i < users.length; i++){

        //if the username provided in the client/postman and the usernam
        //of the current objet is the same
        //galing sa body sa postman
        if(req.body.username == users[i].username){
            //changes the password if users if found in database
            users[i].password = req.body.password;

            //indicator that the password has been changed.
            msg = `User ${req.body.username}'s password has been updated`;
            break;

        } else {
            msg = 'User does not exist.';
        }
    }

    res.send(msg);
    console.log(user);

})



//home page
app.get('/homepage', (req, res) => {
    res.send('welcome to home page');
})

// registered users
app.get('/users', (req, res) => {
    res.send(users);
})

//delete user
app.delete('/delete-user', (req, res) => {
    let msg;

    if (users.length !== 0){
        for (let i = 0; i < users.length; i++){
            if(req.body.username === users[i].username){
                users.splice(users[i], 1);
                console.log(`${users[i]} has been dleted`);
                msg = `Users ${req.body.username} has been deleted`;
                break;
            } 

        }
    } else {
        res.send('User does not exist');
    }

    res.send(msg);
});





//
app.listen(port, () => console.log(`server is running at port ${port}.`));


